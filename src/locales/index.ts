import { i18n } from '@lingui/core';
import { en } from 'make-plural/plurals';

// Declare locales
interface ILocale {
  flag: string;
  plurals: (n: number | string, ord?: boolean) => 'zero' | 'one' | 'two' | 'few' | 'many' | 'other';
  label: string;
}

interface ILocales {
  [locale: string]: ILocale;
}

export const locales: ILocales = {
  en: { flag: 'gb', plurals: en, label: 'English' },
  // vi: { flag: 'vn', plurals: vi, label: 'Vietnamese' },
};

// Load locale data
for (const [key, locale] of Object.entries(locales)) {
  i18n.loadLocaleData(key, { plurals: locale.plurals });
}

export async function fetchLocale(locale = 'en') {
  const { messages } = await import(`locales/translations/${locale}/messages`);
  i18n.load(locale, messages);
  i18n.activate(locale);
}

export function selectLocale(locale: string) {
  window.localStorage.setItem('locale', locale);
  return fetchLocale(locale);
}

export function initLocale() {
  let locale = window.localStorage.getItem('locale') as string;
  if (!Object.keys(locales).includes(locale)) locale = 'en';
  fetchLocale(locale);
}
