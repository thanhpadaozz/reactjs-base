import { Outlet } from 'react-router-dom';
import { CircularProgress, Box } from '@mui/material';

import MainLayout from 'components/layout/MainLayout';
import useWeb3Store from 'stores/useWeb3Store';

const MainOutlet = () => {
  const { isInit } = useWeb3Store();

  return (
    <MainLayout>
      {isInit ? (
        <Box display="flex" flex={1} justifyContent="center" mt={5}>
          <CircularProgress />
        </Box>
      ) : (
        <Outlet />
      )}
    </MainLayout>
  );
};

export default MainOutlet;
