//

import Card from './Card';
import Paper from './Paper';
import Input from './Input';
import Table from './Table';
import Dialog from './Dialog';
import Pagination from './Pagination';
import Button from './Button';
import Tooltip from './Tooltip';
import Backdrop from './Backdrop';
import Typography from './Typography';
import CssBaseline from './CssBaseline';
import Autocomplete from './Autocomplete';
import Link from './Link';

// ----------------------------------------------------------------------

export default function ComponentsOverrides(theme, mode) {
  return Object.assign(
    Card(theme),
    Link(theme),
    Input(theme),
    Paper(theme),
    Dialog(theme),
    Pagination(theme, mode),
    Table(theme, mode),
    Button(theme),
    Tooltip(theme),
    Backdrop(theme),
    Typography(theme),
    CssBaseline(theme),
    Autocomplete(theme),
  );
}
