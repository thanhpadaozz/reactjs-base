export default function Table(theme, mode) {
  return {
    MuiTable: {
      styleOverrides: {
        root: {
          boxShadow: theme.customShadows.z20,
        },
      },
    },
    MuiTableHead: {
      styleOverrides: {
        root: {
          '& tr': {
            '& th': {
              backgroundColor: mode == 'light' ? theme.palette.grey[200] : `${theme.palette.grey[700]}4D`,
            },
          },
          '& th:first-child': {
            borderTopLeftRadius: '8px',
            borderBottomLeftRadius: '8px',
          },
          '& th:last-child': {
            borderTopRightRadius: '8px',
            borderBottomRightRadius: '8px',
          },
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          borderBottom: 'none',
        },
      },
    },
  };
}
