// ----------------------------------------------------------------------

export default function Card(theme) {
  return {
    MuiCard: {
      styleOverrides: {
        root: {
          border: '1px solid rgba(145, 158, 171, 0.24)',
          boxShadow: theme.shadows[2],
          borderRadius: '12px',
          position: 'relative',
          zIndex: 0, // Fix Safari overflow: hidden with border radius

          '& button': {
            boxShadow: 'none',
          },
        },
      },
    },
    MuiCardHeader: {
      defaultProps: {
        titleTypographyProps: { variant: 'h6' },
        subheaderTypographyProps: { variant: 'body2' },
      },
      styleOverrides: {
        root: {
          padding: theme.spacing(3, 3, 0),
        },
      },
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          padding: theme.spacing(3),
        },
      },
    },
  };
}
