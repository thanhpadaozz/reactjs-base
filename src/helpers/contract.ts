// import { ethers } from 'ethers';

import useWeb3Store from 'stores/useWeb3Store';

export const CONTRACT_ADDRESS = String(process.env.REACT_APP_NFT_CONTRACT_ADDRESS);

export const getNFTContractWithSigner = () => {
  const { provider, isConnected } = useWeb3Store();

  if (!provider || !isConnected) return;
  // const contract = new ethers.Contract(CONTRACT_ADDRESS, NFTAbi, provider.getSigner()) as INFT;

  return 1;
};

export const getNFTContractRPC = () => {
  // const provider = new ethers.providers.JsonRpcProvider(CHAIN_RPC_URL);
  // const contract = new ethers.Contract(CONTRACT_ADDRESS, NFTAbi, provider) as INFT;

  return 1;
};
