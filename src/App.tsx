/* eslint-disable @typescript-eslint/ban-ts-comment */
import { useEffect } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { SnackbarProvider } from 'notistack';
import { i18n } from '@lingui/core';
import { I18nProvider } from '@lingui/react';

import './index.css';

import ThemeProvider from './theme';

import { SnackbarConfigurator } from './helpers/notify';

import MainOutlet from './MainOutlet';
import Home from 'pages/Home';
import Profile from 'pages/Profile';
import PrivateOutlet from 'PrivateOutlet';

import InitialWeb3Modal from 'components/web3modal/InitialWeb3Modal';
import { initLocale } from 'locales';
import useAppStore from 'stores/useAppStore';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 0,
      refetchOnWindowFocus: false,
      keepPreviousData: false,
    },
  },
});

const ROUTER = (
  <BrowserRouter>
    <Routes>
      <Route path="" element={<MainOutlet />}>
        <Route path="/" element={<Home />} />
      </Route>

      <Route path="" element={<PrivateOutlet />}>
        <Route path="/profile/:type" element={<Profile />} />
        <Route path="/profile" element={<Navigate to="/profile/info" replace />} />
      </Route>
    </Routes>
  </BrowserRouter>
);

const App = () => {
  const { theme } = useAppStore();

  useEffect(() => {
    initLocale();
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <I18nProvider i18n={i18n}>
        {
          // @ts-ignore
          <SnackbarProvider maxSnack={3} anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}>
            <SnackbarConfigurator />

            <ThemeProvider mode={theme}>{ROUTER}</ThemeProvider>

            <InitialWeb3Modal />
          </SnackbarProvider>
        }
      </I18nProvider>
    </QueryClientProvider>
  );
};

export default App;
