import { useMutation } from 'react-query';
// import { BigNumber } from 'ethers';

import { getNFTContractWithSigner } from 'helpers/contract';

const useRedeemToken = () => {
  const nftContract = getNFTContractWithSigner();

  return useMutation(async () => {
    if (!nftContract) throw new Error('You much connect your wallet');

    // return await (await nftContract.redeem(BigNumber.from(tokenId))).wait();
  });
};

export default useRedeemToken;
