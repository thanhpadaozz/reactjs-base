import { useQuery } from 'react-query';

import { getNFTContractRPC } from 'helpers/contract';

const useGetBaseURI = () => {
  const nftContract = getNFTContractRPC();

  return useQuery(
    'getBaseUri',
    async () => {
      return await nftContract;
    },
    {
      staleTime: Infinity,
      cacheTime: 600000, // 10M
    },
  );
};

export default useGetBaseURI;
