import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import AppBar from '@mui/material/AppBar';
import { Theme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

import ConnectButton from './ConnectButton';
import LocaleSwitcher from './LocaleSwitch';
import ThemeSwitcher from './ThemeSwitcher';

interface IHeaderProps {
  handleOpenSideBar: () => void;
}

const Header = ({ handleOpenSideBar }: IHeaderProps) => {
  const isMobile = useMediaQuery((theme: Theme) => theme.breakpoints.down('md'));

  return (
    <AppBar
      position="sticky"
      sx={{
        backgroundColor: (theme: Theme) => `${theme.palette.background.default}cc`,
        color: (theme: Theme) => theme.palette.primary.main,
        backdropFilter: 'blur(6px)',
        boxShadow: 'rgb(4 17 29 / 25%) 0px 0px 8px 0px',
      }}
    >
      <Toolbar sx={{ a: { textDecoration: 'none' }, height: { lg: 72 } }}>
        {isMobile && (
          <IconButton edge="start" onClick={() => handleOpenSideBar()}>
            <MenuIcon />
          </IconButton>
        )}

        <Box display="flex" flexGrow={1} />

        {!isMobile && (
          <>
            <Box sx={{ ml: 2, display: 'inline' }}>
              <ConnectButton />
            </Box>
            <Box sx={{ ml: 2, display: 'inline' }}>
              <ThemeSwitcher />
            </Box>

            <Box sx={{ ml: 2, display: 'inline' }}>
              <LocaleSwitcher />
            </Box>
          </>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Header;
