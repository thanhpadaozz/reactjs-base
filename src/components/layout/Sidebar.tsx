import { Drawer, Box } from '@mui/material';

import NavContent from './NavContent';

const Sidebar = () => {
  return (
    <Drawer variant="permanent" anchor="left" sx={{ width: '270px' }}>
      <Box sx={{ width: '270px', px: 3 }}>
        <NavContent />
      </Box>
    </Drawer>
  );
};

export default Sidebar;
