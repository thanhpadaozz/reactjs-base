import { useState, useEffect } from 'react';

import Box from '@mui/material/Box';
import { useMediaQuery } from '@mui/material';
import { Theme } from '@mui/material/styles';

import Header from './Header';
import SidebarMobile from './SidebarMobile';
import Sidebar from './Sidebar';

const MainLayout = ({ children }: any) => {
  const isMobile = useMediaQuery((theme: Theme) => theme.breakpoints.down('md'));
  const [openSideBarMobile, setOpenSideBarMobile] = useState<boolean>(false);

  useEffect(() => {
    if (isMobile) {
      setOpenSideBarMobile(false);
    }
  }, [isMobile]);

  const handleOpenSideBarMobile = () => {
    setOpenSideBarMobile(true);
  };

  const handleCloseSideBarMobile = () => {
    setOpenSideBarMobile(false);
  };

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          minHeight: { xs: 'calc(100vh - 56px)', md: 'calc(100vh - 72px)' },
        }}
      >
        <Box display="flex" flex={1}>
          <SidebarMobile openSideBar={openSideBarMobile} handleCloseSideBar={handleCloseSideBarMobile} />
          {!isMobile && <Sidebar />}

          <Box display="flex" flex={1} flexDirection="column">
            <Header handleOpenSideBar={handleOpenSideBarMobile} />
            <Box component="main" flexGrow={1} pb={12} pt={4}>
              {children}
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default MainLayout;
