import { i18n } from '@lingui/core';
import { useState, useRef, useMemo } from 'react';
import { IconButton, MenuItem, Box, Stack } from '@mui/material';
import { alpha } from '@mui/material/styles';

import FlagIcon from 'helpers/flagicon';
import { locales, selectLocale } from 'locales';
import CustomPopover from 'components/common/CustomPopover';

const LocaleSwitcher = () => {
  const anchorRef = useRef(null);
  const [open, setOpen] = useState(false);

  const [language, setLanguage] = useState(i18n.locale);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const currentLocale = useMemo(() => locales[language], [locales]);

  const handleChangeLanguage = (locale: string) => {
    setLanguage(locale);
    selectLocale(locale);
  };

  return (
    <>
      <IconButton
        ref={anchorRef}
        onClick={handleOpen}
        sx={{
          padding: 1,
          width: 40,
          height: 40,
          ...(open && {
            bgcolor: (theme) => alpha(theme.palette.primary.main, theme.palette.action.focusOpacity),
          }),
        }}
      >
        <FlagIcon code={currentLocale.flag} />
      </IconButton>

      <CustomPopover
        open={open}
        onClose={handleClose}
        anchorEl={anchorRef.current}
        sx={{
          mt: 1.5,
          ml: 0.75,
          width: 180,
          '& .MuiMenuItem-root': { px: 1, typography: 'body2', borderRadius: 0.75 },
        }}
      >
        <Stack spacing={0.75}>
          {Object.keys(locales).map((locale) => (
            <MenuItem key={locale} selected={locale == language} onClick={() => handleChangeLanguage(locale)}>
              <Box sx={{ width: 28, mr: 1 }}>
                <FlagIcon code={locales[locale].flag} />
              </Box>
              {locales[locale].label}
            </MenuItem>
          ))}
        </Stack>
      </CustomPopover>
    </>
  );
};

export default LocaleSwitcher;
