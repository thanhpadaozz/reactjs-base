import Drawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';

import NavContent from './NavContent';

interface ISidebarMobileProps {
  openSideBar: boolean;
  handleCloseSideBar: () => void;
}

const SidebarMobile = ({ openSideBar, handleCloseSideBar }: ISidebarMobileProps) => (
  <Drawer anchor="left" open={openSideBar} onClose={handleCloseSideBar}>
    <Box sx={{ width: '270px', px: 2 }}>
      <NavContent />
    </Box>
  </Drawer>
);

export default SidebarMobile;
