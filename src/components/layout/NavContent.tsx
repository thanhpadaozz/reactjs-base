import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router';

import { Box, Button } from '@mui/material';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import List from '@mui/material/List';

import HectagonLightIcon from 'assets/icons/HectagonLightIcon.png';
import HectagonDarkIcon from 'assets/icons/HectagonDarkIcon.png';
import ProfileIcon from 'assets/icons/profileIcon.png';

import useAppStore from 'stores/useAppStore';
import useWeb3Store from 'stores/useWeb3Store';
import { shortenAddress } from 'helpers/utils';

const NavContent = () => {
  const { theme } = useAppStore();
  const navigate = useNavigate();
  const { isConnected, accountAddress } = useWeb3Store();

  const TABS = [
    {
      title: 'Profile',
      icon: ProfileIcon,
      needConnected: true,
      link: '/profile/info',
    },
  ];

  return (
    <Box display="flex" flexDirection="column" alignItems="center" width="100%">
      <Link to="/">
        <Box
          component="img"
          src={theme == 'light' ? HectagonLightIcon : HectagonDarkIcon}
          sx={{ maxWidth: '100%', mt: 5.5, mb: 5 }}
        />
      </Link>

      {isConnected && accountAddress && (
        <Button
          sx={{
            backgroundColor: (themeStyle) =>
              theme == 'light' ? themeStyle.palette.grey[200] : themeStyle.palette.grey[700],
            color: (theme) => theme.palette.text.primary,
            width: '80%',
            fontWeight: 100,
            mb: 2,
          }}
        >
          {shortenAddress(accountAddress)}
        </Button>
      )}

      <List sx={{ width: '100%' }}>
        {TABS.map((tab) => {
          if (tab.needConnected && !isConnected) return null;

          return (
            <ListItem key={tab.title} disablePadding>
              <ListItemButton onClick={() => navigate(tab.link)}>
                <ListItemIcon>
                  <Box component="img" src={tab.icon} />
                </ListItemIcon>
                <ListItemText primary={tab.title} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
    </Box>
  );
};

export default NavContent;
