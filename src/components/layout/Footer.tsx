import { Typography, Stack, Box } from '@mui/material';
import { useTheme } from '@mui/material/styles';

import { ReactComponent as Twitter } from 'assets/icons/twitterDark.svg';
import { ReactComponent as Telegram } from 'assets/icons/telDark.svg';
import { ReactComponent as Medium } from 'assets/icons/mediumDark.svg';
import { ReactComponent as Discord } from 'assets/icons/discordDark.svg';

const Footer = () => {
  const theme = useTheme();

  return (
    <Box
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      sx={{ mb: 5 }}
      flexDirection={{ xs: 'column', sm: 'row' }}
    >
      <Typography fontSize="1rem" sx={{ mb: { xs: 1.2, sm: 0 } }}>
        Hectagon © 2022 All Rights Reserved
      </Typography>

      <Stack
        direction="row"
        spacing={2}
        sx={{
          div: {
            display: 'grid',
            placeItems: 'center',
            background: '#E6E6E6',
            width: 32,
            height: 32,
            borderRadius: '50%',
            cursor: 'pointer',
            [theme.breakpoints.up('xs')]: {
              alignItems: 'center',
            },
          },
          a: {
            display: 'flex',
            alignItems: 'center',
          },
        }}
      >
        <div>
          <a href="https://t.me/HectagonDAGPGroup" target="_blank">
            <Telegram />
          </a>
        </div>
        <div>
          <a href="https://twitter.com/HectagonGrowth" target="_blank">
            <Twitter />
          </a>
        </div>
        <div>
          <a href="https://medium.com/@hectagongrowthprotocol" target="_blank">
            <Medium />
          </a>
        </div>
        <div>
          <a href="https://discord.com/invite/N9dF9j5ysk" target="_blank">
            <Discord />
          </a>
        </div>
      </Stack>
    </Box>
  );
};

export default Footer;
