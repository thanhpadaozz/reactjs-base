import { Link, useMatch, useResolvedPath } from 'react-router-dom';
import type { LinkProps } from 'react-router-dom';

import MuiLink from '@mui/material/Link';
import { Theme } from '@mui/material';

import useAppStore from 'stores/useAppStore';

const CustomLink = ({ children, to, ...props }: LinkProps) => {
  const resolved = useResolvedPath(to);
  const { theme } = useAppStore();
  const match = useMatch({ path: resolved.pathname, end: true });

  const LinkStyle = {
    color: (themeMui: Theme) => (match ? themeMui.palette.primary.main : themeMui.palette.text.primary),
    fontWeight: '600',
    p: 1,
    borderRadius: '12px',
    fontSize: { xs: '1.4rem', md: '1rem' },
    '&:hover': {
      backgroundColor: (themeMui: Theme) =>
        theme === 'dark' ? `${themeMui.palette.primary.light}47` : `${themeMui.palette.primary.main}14`,
      color: (themeMui: Theme) => (theme === 'dark' ? themeMui.palette.primary.light : themeMui.palette.primary.dark),
    },
    mr: 3,
    ml: 1,
  };

  return (
    <Link to={to} {...props}>
      <MuiLink sx={LinkStyle}>{children}</MuiLink>
    </Link>
  );
};

export default CustomLink;
