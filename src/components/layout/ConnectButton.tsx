import { shortenAddress } from 'helpers/utils';
import { t } from '@lingui/macro';
import { Button, Dialog, DialogContent, DialogContentText } from '@mui/material';
import { useState } from 'react';

import SecondaryButton from 'components/common/SecondaryButton';

import useWeb3Store from 'stores/useWeb3Store';
import useAppStore from 'stores/useAppStore';

const ConnectButton = () => {
  const { theme } = useAppStore();
  const { accountAddress, loadWeb3Modal, isConnected, logoutOfWeb3Modal } = useWeb3Store();
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button
        variant="contained"
        sx={{
          boxShadow: 'none',
          width: 150,
        }}
        onClick={() => {
          if (isConnected) {
            setOpen(true);
          } else {
            loadWeb3Modal();
          }
        }}
      >
        {!isConnected ? `${t`Connect Wallet`}` : accountAddress && shortenAddress(accountAddress)}
      </Button>

      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogContent
          sx={{
            width: 300,
            display: 'flex',
            alignItems: 'center',
            textAlign: 'center',
            flexDirection: 'column',
            my: 2,
          }}
        >
          <DialogContentText>{shortenAddress(accountAddress || '')}</DialogContentText>
          <DialogContentText sx={{ my: 2 }}>{t`Wallet connected`}</DialogContentText>

          <SecondaryButton
            sx={{
              boxShadow: 'none',
              width: 150,
              backgroundColor: (themeMui) =>
                theme === 'dark' ? `${themeMui.palette.grey[700]} !important` : `${themeMui.palette.grey[300]}`,
            }}
            onClick={logoutOfWeb3Modal}
          >
            <>{t`Disconnect`}</>
          </SecondaryButton>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default ConnectButton;
