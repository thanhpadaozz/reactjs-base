import { Button, ButtonProps, Theme } from '@mui/material';
import { SxProps } from '@mui/system';

import useAppStore from 'stores/useAppStore';

interface ISecondaryButton extends ButtonProps {
  children: React.ReactElement;
  sx?: SxProps<Theme>;
}

const SecondaryButton = ({ children, sx, ...ButtonProps }: ISecondaryButton) => {
  const { theme } = useAppStore();

  return (
    <Button
      sx={{
        boxShadow: 'none',
        color: (themeMui) => (theme === 'dark' ? themeMui.palette.grey[100] : themeMui.palette.grey[700]),
        '&:hover': {
          backgroundColor: (themeMui) =>
            theme === 'dark' ? `${themeMui.palette.grey[600]} !important` : `${themeMui.palette.grey[400]}`,
        },
        ...sx,
      }}
      {...ButtonProps}
    >
      {children}
    </Button>
  );
};

export default SecondaryButton;
