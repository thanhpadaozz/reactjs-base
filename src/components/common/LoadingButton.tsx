import { LoadingButton, LoadingButtonProps } from '@mui/lab';
import Box from '@mui/material/Box';

interface ICustomLoadingButton extends LoadingButtonProps {
  position?: 'start' | 'end' | 'center';
}

const CustomLoadingButton = ({
  children,
  loading: isLoading,
  position = 'start',
  ...buttonProps
}: ICustomLoadingButton) => (
  <LoadingButton sx={{ boxShadow: 'none' }} {...buttonProps} loadingPosition={position} loading={isLoading}>
    {position === 'start' ? (
      <>
        <Box
          sx={{
            transition: 'all .3s cubic-bezier(.645,.045,.355,1)',
            width: isLoading ? 22 : 0,
          }}
        />

        {children}
      </>
    ) : (
      <>
        {children}
        <Box
          sx={{
            transition: 'all .3s cubic-bezier(.645,.045,.355,1)',
            width: isLoading && position != 'center' ? 22 : 0,
          }}
        />
      </>
    )}
  </LoadingButton>
);

export default CustomLoadingButton;
