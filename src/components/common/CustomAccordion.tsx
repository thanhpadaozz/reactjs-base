import Accordion, { AccordionProps } from '@mui/material/Accordion';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import AccordionSummary from '@mui/material/AccordionSummary';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

interface ICustomAccordion extends AccordionProps {
  children: React.ReactElement;
  title: string;
}

const CustomAccordion = ({ title, children, ...props }: ICustomAccordion) => {
  return (
    <Accordion
      disableGutters
      sx={{
        border: (theme) => `1px solid ${theme.palette.divider}`,
        borderRadius: '0.5rem',
        '&::before': { opacity: '0 !important' },
      }}
      {...props}
    >
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <MoreVertIcon />
          <Typography variant="h5" sx={{ ml: 1 }}>
            {title}
          </Typography>
        </Box>
      </AccordionSummary>
      {children}
    </Accordion>
  );
};

export default CustomAccordion;
