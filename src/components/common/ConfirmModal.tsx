import * as React from 'react';
import { useState } from 'react';
import { t } from '@lingui/macro';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import SecondaryButton from 'components/common/SecondaryButton';

const ConfirmModal = ({
  title,
  onOk,
  content,
  children,
}: {
  children: React.ReactElement;
  content?: React.ReactElement;
  title: string;
  onOk: () => void;
}) => {
  const [open, setOpen] = useState(false);
  const handleConfirm = async () => {
    onOk();
    setOpen(false);
  };
  const ActionElement = React.cloneElement(children, {
    onClick: () => setOpen(true),
  });
  return (
    <>
      {ActionElement}
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle sx={{ minWidth: 300 }}>{title}</DialogTitle>
        {content && (
          <DialogContent>
            <DialogContentText id="alert-dialog-description">{content}</DialogContentText>
          </DialogContent>
        )}
        <DialogActions sx={{ px: 3, pb: 2 }}>
          <SecondaryButton onClick={() => setOpen(false)}>
            <>{t`Cancel`}</>
          </SecondaryButton>
          <Button variant="contained" onClick={handleConfirm}>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ConfirmModal;
