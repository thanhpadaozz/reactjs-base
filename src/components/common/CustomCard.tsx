import { Card, CardHeader, CardContent, CardHeaderProps, Theme } from '@mui/material';
import { SxProps } from '@mui/system';

interface ICustomCard {
  children: React.ReactElement;
  title?: string;
  cardHeader?: CardHeaderProps;
  sx?: SxProps<Theme>;
}

const CustomCard = ({ sx, title, children, cardHeader }: ICustomCard) => {
  return (
    <Card sx={sx}>
      {(title || cardHeader) && (
        <CardHeader title={title} sx={{ '& .MuiCardHeader-avatar': { mr: 1 } }} {...cardHeader} />
      )}
      <CardContent>{children}</CardContent>
    </Card>
  );
};

export default CustomCard;
