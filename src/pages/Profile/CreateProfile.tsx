import { useQueryClient } from 'react-query';
import { Box, Card, Typography, TextField } from '@mui/material';
import { useForm } from 'react-hook-form';
import { t } from '@lingui/macro';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import { shortenAddress } from 'helpers/utils';
import useWeb3Store from 'stores/useWeb3Store';
import useAppStore from 'stores/useAppStore';
import useRegister from 'data/useRegister';
import { handleErrorMessage } from 'helpers/error';

import { ReactComponent as CopyClipboard } from 'assets/icons/copy.svg';
import LoadingButton from 'components/common/LoadingButton';
import SecondaryButton from 'components/common/SecondaryButton';

type RegisterForm = {
  username: string;
};

const CreateProfile = () => {
  const { provider, accountAddress } = useWeb3Store();
  const { setAccessToken } = useAppStore();
  const queryClient = useQueryClient();
  const { mutate: registerProfile, isLoading: loadingRegister } = useRegister();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterForm>({
    mode: 'onSubmit',
  });

  const handleRegister = async (values: RegisterForm) => {
    if (accountAddress) {
      const res = await provider?.getSigner().signMessage(accountAddress);
      if (res && accountAddress) {
        registerProfile(
          { signature: res, address: accountAddress, username: values.username },
          {
            onSuccess: (data) => {
              queryClient.setQueryData('get me', data?.user);
              setAccessToken(data?.accessToken?.token);
            },
            onError: (e) => {
              handleErrorMessage(e);
            },
          },
        );
      }
    }
  };

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <Card sx={{ p: 3 }}>
        <Typography variant="h3" align="center">
          {t`Create profile`}
        </Typography>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 1 }}>
          <Typography align="center" sx={{ mr: 1 }}>
            {shortenAddress(accountAddress || '')}
          </Typography>
          <CopyToClipboard text={accountAddress || ''}>
            <Box display="flex" sx={{ cursor: 'pointer' }}>
              <CopyClipboard />
            </Box>
          </CopyToClipboard>
        </Box>

        <Typography align="center" sx={{ pt: 2 }}>
          {t`Create a profile to get chance to join community bounty pool`}
        </Typography>
        <form onSubmit={handleSubmit(handleRegister)}>
          <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
            <TextField
              label="Username"
              autoFocus
              sx={{ width: 400 }}
              helperText={errors && errors.username?.message}
              error={!!errors.username}
              inputProps={{
                ...register('username', {
                  required: { value: true, message: 'Username is required!' },
                }),
              }}
            />
          </Box>

          <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
            <SecondaryButton sx={{ mr: 3 }} type="button" size="large">
              <>{t`Cancel`}</>
            </SecondaryButton>

            <LoadingButton
              loading={loadingRegister}
              variant="contained"
              size="large"
              type="submit"
            >{t`Confirm`}</LoadingButton>
          </Box>
        </form>
      </Card>
    </Box>
  );
};

export default CreateProfile;
