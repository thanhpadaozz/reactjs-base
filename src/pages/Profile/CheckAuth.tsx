import { CircularProgress, Box } from '@mui/material';

import SignIn from './SignIn';
import useAppStore from 'stores/useAppStore';
import useGetMe from 'data/useGetMe';
import useWeb3Store from 'stores/useWeb3Store';

const CheckAuth = ({ children }: { children: any }) => {
  const { accessToken } = useAppStore();
  const { data, isLoading, isError } = useGetMe();
  const { accountAddress } = useWeb3Store();

  if (!accessToken || isError || (data && !data?.wallets.find((wallet: any) => wallet.address == accountAddress)))
    return <SignIn />;

  if (isLoading) {
    return (
      <Box sx={{ display: 'flex', justifyContent: 'center' }}>
        <CircularProgress />
      </Box>
    );
  }

  return <>{children}</>;
};

export default CheckAuth;
