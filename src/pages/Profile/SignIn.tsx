/* eslint-disable @typescript-eslint/ban-ts-comment */
import { useQueryClient } from 'react-query';
import { useEffect } from 'react';
import { Container, Box, Button, CircularProgress, Alert } from '@mui/material';

import useWeb3Store from 'stores/useWeb3Store';
import useGetNonce from 'data/useGetNonce';
import useSignIn from 'data/useSignIn';

import CreateProfile from './CreateProfile';
import { handleErrorMessage } from 'helpers/error';
import useAppStore from 'stores/useAppStore';

const SignIn = () => {
  const queryClient = useQueryClient();
  const { setAccessToken } = useAppStore();
  const { provider, accountAddress } = useWeb3Store();
  const { data, error, isLoading } = useGetNonce();
  const { mutate: signIn, isLoading: loadingSignIn } = useSignIn();

  useEffect(() => {
    setAccessToken('');
  }, []);

  const handleSign = async () => {
    if (data?.nonce) {
      const res = await provider?.getSigner().signMessage(data?.nonce);
      if (res && accountAddress) {
        signIn(
          { signature: res, address: accountAddress },
          {
            onSuccess: (data) => {
              queryClient.setQueryData('get me', data?.user);
              setAccessToken(data?.accessToken?.token);
            },
            onError: (e) => {
              handleErrorMessage(e);
            },
          },
        );
      }
    }
  };

  useEffect(() => {
    handleSign();
  }, [data?.nonce]);

  // @ts-ignore
  if (error && error?.response?.status == 404) {
    return <CreateProfile />;
  }

  if (error) {
    return (
      <Container>
        <Box display="flex" flex={1} width="100%" justifyContent="center">
          <Alert sx={{ mt: 5 }} severity="error">
            Something error!
          </Alert>
        </Box>
      </Container>
    );
  }
  return (
    <Container>
      <Box display="flex" flex={1} width="100%" alignItems="center" flexDirection="column" pt={5}>
        {isLoading ? (
          <CircularProgress />
        ) : (
          <>
            Please sign message authenticate
            <Button sx={{ px: 6, mt: 2 }} variant="contained" disabled={!data || loadingSignIn} onClick={handleSign}>
              Sign To Login
            </Button>
          </>
        )}
      </Box>
    </Container>
  );
};

export default SignIn;
