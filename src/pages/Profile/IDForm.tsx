import * as React from 'react';
import { useState } from 'react';
import { t } from '@lingui/macro';

import { Box, Typography, Card, Button } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { styled } from '@mui/material/styles';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import CheckIcon from '@mui/icons-material/Check';
import CameraAltIcon from '@mui/icons-material/CameraAlt';

import demoFrontCard from 'assets/DemoFrontCard.png';
import demoBackCard from 'assets/DemoBackCard.png';
import demoSelfie from 'assets/DemoSelfie.png';
import LoadingButton from 'components/common/LoadingButton';
import SecondaryButton from 'components/common/SecondaryButton';

const IDForm = () => {
  const [formStatus, setFormStatus] = useState('');
  const [value, setValue] = React.useState<Date | null>(null);

  const Input = styled('input')({
    display: 'none',
  });

  if (formStatus === 'edit-birth') {
    return (
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" sx={{ alignItems: 'center' }}>
          <IconButton aria-label="Back" onClick={() => setFormStatus('')}>
            <ArrowBackIosNewIcon />
          </IconButton>
          <Typography variant="h6">Phone Number</Typography>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              value={value}
              onChange={(newValue) => {
                setValue(newValue);
              }}
              label="Date of birth"
              renderInput={(params) => <TextField {...params} fullWidth autoFocus />}
            />
          </LocalizationProvider>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <SecondaryButton sx={{ mr: 3 }} type="button" onClick={() => setFormStatus('')}>
            <>{t`Cancel`}</>
          </SecondaryButton>

          <LoadingButton variant="contained">{t`Confirm`}</LoadingButton>
        </Box>
      </Card>
    );
  }

  if (formStatus === 'edit-card') {
    return (
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" sx={{ alignItems: 'center' }}>
          <IconButton aria-label="Back" onClick={() => setFormStatus('')}>
            <ArrowBackIosNewIcon />
          </IconButton>
          <Typography variant="h6">Passport/ National ID</Typography>
        </Box>

        <Box display="flex" sx={{ alignItems: 'center' }}>
          <CheckIcon sx={{ mx: 3, color: '#008E6C' }} />
          <Typography sx={{ color: '#999999' }}>Fully in frame, all information are clearly visible</Typography>
        </Box>

        <Box display="flex" sx={{ alignItems: 'center', mb: 3 }}>
          <CheckIcon sx={{ mx: 3, color: '#008E6C' }} />
          <Typography sx={{ color: '#999999' }}>File in format jpg/jpeg/png format</Typography>
        </Box>

        <Box>
          <label htmlFor="icon-button-file">
            <Input accept="image/*" id="icon-button-file" type="file" />
            <Button
              aria-label="upload picture"
              component="span"
              sx={{
                width: '100%',
                background: 'none',
                border: '2px dashed #E6E6E6',
                py: 10,
                borderRadius: '8px',
                '&:hover': { background: 'none' },
              }}
            >
              <CameraAltIcon sx={{ color: '#E6E6E6' }} />
            </Button>
          </label>

          <Typography sx={{ py: 1 }}>Front card</Typography>
        </Box>

        <Box>
          <Box component="img" src={demoBackCard} sx={{ width: '100%' }} />
          <Typography sx={{ py: 1 }}>Back card</Typography>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <SecondaryButton sx={{ mr: 3 }} type="button" onClick={() => setFormStatus('')}>
            <>{t`Cancel`}</>
          </SecondaryButton>

          <LoadingButton variant="contained">{t`Confirm`}</LoadingButton>
        </Box>
      </Card>
    );
  }

  if (formStatus === 'edit-selfie') {
    return (
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" sx={{ alignItems: 'center' }}>
          <IconButton aria-label="Back" onClick={() => setFormStatus('')}>
            <ArrowBackIosNewIcon />
          </IconButton>
          <Typography variant="h6">Selfie picture</Typography>
        </Box>

        <Box sx={{ mt: 2 }}>
          <label htmlFor="icon-button-file">
            <Input accept="image/*" id="icon-button-file" type="file" />
            <Button
              aria-label="upload picture"
              component="span"
              sx={{
                width: '100%',
                background: 'none',
                border: '2px dashed #E6E6E6',
                py: 10,
                borderRadius: '8px',
                '&:hover': { background: 'none' },
              }}
            >
              <CameraAltIcon sx={{ color: '#E6E6E6' }} />
            </Button>
          </label>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <SecondaryButton sx={{ mr: 3 }} type="button" onClick={() => setFormStatus('')}>
            <>{t`Cancel`}</>
          </SecondaryButton>

          <LoadingButton variant="contained">{t`Confirm`}</LoadingButton>
        </Box>
      </Card>
    );
  }

  return (
    <>
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" justifyContent="space-between" sx={{ borderBottom: 1, borderColor: 'divider', pb: 1 }}>
          <Box sx={{ alignItems: 'center', mt: 2 }}>
            <Typography variant="h6">Passport/National ID</Typography>
            <Box display="flex">
              <Box component="img" src={demoFrontCard} sx={{ mr: 2 }} />
              <Box component="img" src={demoBackCard} />
            </Box>
          </Box>

          <Button sx={{ height: 'fit-content', mt: 1.5 }} variant="text" onClick={() => setFormStatus('edit-card')}>
            Edit
          </Button>
        </Box>

        <Box display="flex" justifyContent="space-between" sx={{ borderBottom: 1, borderColor: 'divider', pb: 1 }}>
          <Box>
            <Box sx={{ alignItems: 'center', mt: 2 }}>
              <Typography variant="h6">Selfie picture</Typography>
              <Box component="img" src={demoSelfie} />
            </Box>
          </Box>

          <Button sx={{ height: 'fit-content', mt: 1.5 }} variant="text" onClick={() => setFormStatus('edit-selfie')}>
            Edit
          </Button>
        </Box>

        <Box display="flex" justifyContent="space-between">
          <Box>
            <Box sx={{ alignItems: 'center', mt: 2 }}>
              <Typography variant="h6">Date of birth</Typography>
              <Typography sx={{ color: '#999999' }}>12/12/1998</Typography>
            </Box>
          </Box>

          <Button sx={{ height: 'fit-content', mt: 1.5 }} variant="text" onClick={() => setFormStatus('edit-birth')}>
            Edit
          </Button>
        </Box>
      </Card>
    </>
  );
};

export default IDForm;
