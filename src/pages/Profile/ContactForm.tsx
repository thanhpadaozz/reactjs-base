import { useState } from 'react';
import { t } from '@lingui/macro';

import { Box, Typography, Card, Button, TextField } from '@mui/material';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import { styled } from '@mui/material/styles';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload';

import demoBill from 'assets/DemoBill.png';
import LoadingButton from 'components/common/LoadingButton';
import SecondaryButton from 'components/common/SecondaryButton';

const ContactForm = () => {
  const [formStatus, setFormStatus] = useState('');

  const Input = styled('input')({
    display: 'none',
  });

  if (formStatus === 'edit-phone') {
    return (
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" sx={{ alignItems: 'center' }}>
          <IconButton aria-label="Back" onClick={() => setFormStatus('')}>
            <ArrowBackIosNewIcon />
          </IconButton>
          <Typography variant="h6">Phone Number</Typography>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <TextField
            label="Phone Number"
            autoFocus
            fullWidth
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Typography sx={{ borderRight: 1, borderColor: 'divider', pr: 6 }}>+84</Typography>
                </InputAdornment>
              ),
            }}
          />
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <SecondaryButton sx={{ mr: 3 }} type="button" onClick={() => setFormStatus('')}>
            <>{t`Cancel`}</>
          </SecondaryButton>

          <LoadingButton variant="contained">{t`Confirm`}</LoadingButton>
        </Box>
      </Card>
    );
  }

  if (formStatus === 'edit-address') {
    return (
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" sx={{ alignItems: 'center' }}>
          <IconButton aria-label="Back" onClick={() => setFormStatus('')}>
            <ArrowBackIosNewIcon />
          </IconButton>
          <Typography variant="h6">Address and more</Typography>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <TextField label="Address" autoFocus fullWidth />
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <TextField
            label="Upload Bill/ Bank statement to proof your address"
            fullWidth
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <label htmlFor="icon-button-file">
                    <Input accept="image/*" id="icon-button-file" type="file" />
                    <IconButton color="primary" aria-label="upload picture" component="span">
                      <DriveFolderUploadIcon />
                    </IconButton>
                  </label>
                </InputAdornment>
              ),
            }}
          />
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <SecondaryButton sx={{ mr: 3 }} type="button" onClick={() => setFormStatus('')}>
            <>{t`Cancel`}</>
          </SecondaryButton>

          <LoadingButton variant="contained">{t`Confirm`}</LoadingButton>
        </Box>
      </Card>
    );
  }

  return (
    <>
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" justifyContent="space-between" sx={{ borderBottom: 1, borderColor: 'divider', pb: 1 }}>
          <Box sx={{ alignItems: 'center', mt: 2 }}>
            <Typography variant="h6">Phone number</Typography>
            <Typography sx={{ color: '#999999' }}>(+84)961099456</Typography>
          </Box>

          <Button sx={{ height: 'fit-content', mt: 1.5 }} variant="text" onClick={() => setFormStatus('edit-phone')}>
            Edit
          </Button>
        </Box>

        <Box display="flex" justifyContent="space-between">
          <Box>
            <Box sx={{ alignItems: 'center', mt: 2 }}>
              <Typography variant="h6">Address and more </Typography>
              <Typography sx={{ color: '#999999' }}>Hoan Kiem, Ha Noi</Typography>
              <Box component="img" src={demoBill} />
            </Box>
          </Box>

          <Button sx={{ height: 'fit-content', mt: 1.5 }} variant="text" onClick={() => setFormStatus('edit-address')}>
            Edit
          </Button>
        </Box>
      </Card>
    </>
  );
};

export default ContactForm;
