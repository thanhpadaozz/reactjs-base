import * as React from 'react';
import { useForm } from 'react-hook-form';
import { t } from '@lingui/macro';

import { Box, Typography, Link, Stack } from '@mui/material';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import IconButton from '@mui/material/IconButton';
import avatar from 'assets/icons/avatar.png';
import SecondaryButton from 'components/common/SecondaryButton';
import LoadingButton from 'components/common/LoadingButton';

type RewardForm = {
  claimAll: string;
};

const Reward = () => {
  const [value, setValue] = React.useState<Date | null>(null);
  const { handleSubmit } = useForm<RewardForm>({
    mode: 'onSubmit',
  });

  const RewardCard = () => {
    return (
      <Box
        display="flex"
        sx={{
          justifyContent: 'space-between',
          alignItems: 'center',
          mt: 3,
          pb: 2,
          borderBottom: 1,
          borderColor: 'divider',
        }}
      >
        <Box>
          <Box display="flex">
            <Box component="img" src={avatar} />

            <Box sx={{ alignItems: 'center', mt: 2, ml: 3 }}>
              <Typography color="text">
                <Link href="#" target="_blank">
                  Break The Ice
                </Link>
              </Typography>
              <Box display="flex" sx={{ alignItems: 'center' }}>
                <Typography color="text">You reach KYC level1, you was rewarded 0.1 HECTA</Typography>
                <IconButton aria-label="Add">
                  <OpenInNewIcon />
                </IconButton>
              </Box>
            </Box>
          </Box>

          <Typography color="#999999">12 April 2022</Typography>
        </Box>

        <Box>
          <Box display="flex" justifyContent="flex-end">
            <LoadingButton
              variant="outlined"
              sx={{
                boxShadow: 'none',
                height: 'fit-content',
                mb: 2,
              }}
            >
              <>{t`Claim`}</>
            </LoadingButton>
          </Box>

          <Typography color="#999999">Claimed at 16 May 2022</Typography>
        </Box>
      </Box>
    );
  };

  const onSubmit = (data: RewardForm) => {
    console.log(data);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{ mb: 2 }}>
          <Box>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                value={value}
                onChange={(newValue) => {
                  setValue(newValue);
                }}
                label="From"
                renderInput={(params) => <TextField {...params} sx={{ mr: 2 }} />}
              />
            </LocalizationProvider>

            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                value={value}
                onChange={(newValue) => {
                  setValue(newValue);
                }}
                label="To"
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </Box>

          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <SecondaryButton sx={{ mr: 3 }} type="button" size="large">
              <>{t`Clear`}</>
            </SecondaryButton>

            <LoadingButton variant="contained" size="large">{t`Apply`}</LoadingButton>
          </Box>
        </Stack>

        <FormControl>
          <RadioGroup row name="claimAll">
            <FormControlLabel value="all" control={<Radio />} label="All" />
            <FormControlLabel value="claimable" control={<Radio />} label="Claimable" />
          </RadioGroup>
        </FormControl>
      </form>

      <RewardCard />
      <RewardCard />
      <RewardCard />
    </>
  );
};

export default Reward;
