import { useParams, useNavigate, Navigate } from 'react-router-dom';

import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import { t } from '@lingui/macro';

import PersonalInfomation from './PersonalInformation';

import Reward from './Reward';
import SocialAccount from './SocialAccount';
import CheckAuth from './CheckAuth';

const TYPE_OF_PROFILE = ['info', 'social', 'reward', 'wallet', 'portfolio'];

const Profile = () => {
  const { type } = useParams();
  const navigate = useNavigate();

  if (!TYPE_OF_PROFILE.includes(type || '')) {
    return <Navigate to="/profile/info" replace></Navigate>;
  }

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <Card sx={{ p: 3, width: '90%', maxWidth: '1000px' }}>
        <TabContext value={type || 'info'}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList variant="fullWidth">
              <Tab label={t`Personal Information`} value="info" onClick={() => navigate('/profile/info')} />
              <Tab label={t`Social accounts`} value="social" onClick={() => navigate('/profile/social')} />
              <Tab label={t`Reward`} value="reward" onClick={() => navigate('/profile/reward')} />
              <Tab label={t`Wallets`} value="wallet" onClick={() => navigate('/profile/wallet')} />
              <Tab label={t`Portfolio`} value="portfolio" onClick={() => navigate('/profile/portfolio')} />
            </TabList>
          </Box>

          <TabPanel value="info">
            <PersonalInfomation />
          </TabPanel>
          <TabPanel value="social">
            <SocialAccount />
          </TabPanel>
          <TabPanel value="reward">
            <Reward />
          </TabPanel>
          <TabPanel value="wallet">Wallets</TabPanel>
          <TabPanel value="portfolio">Portfolio</TabPanel>
        </TabContext>
      </Card>
    </Box>
  );
};

export default () => (
  <CheckAuth>
    <Profile />
  </CheckAuth>
);
