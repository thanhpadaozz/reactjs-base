import { useState } from 'react';
import { t } from '@lingui/macro';

import { Box, Typography, Card, Button, TextField } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import IconButton from '@mui/material/IconButton';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';

import LoadingButton from 'components/common/LoadingButton';
import SecondaryButton from 'components/common/SecondaryButton';
import { COUNTRIES } from 'enum';
import FlagIcon from 'helpers/flagicon';

const InfoForm = () => {
  const [formStatus, setFormStatus] = useState('');
  const [onSendOTP, setOnSendOTP] = useState(false);

  const handleCancelForm = () => {
    setOnSendOTP(false);
    setFormStatus('');
  };

  if (formStatus === 'edit-email') {
    return (
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" sx={{ alignItems: 'center' }}>
          <IconButton aria-label="Back" onClick={handleCancelForm}>
            <ArrowBackIosNewIcon />
          </IconButton>
          <Typography variant="h6">Email</Typography>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <TextField label="Email" autoFocus fullWidth />
        </Box>

        <Box display="flex" justifyContent="flex-end">
          {onSendOTP ? (
            <Button variant="text">Resend OTP</Button>
          ) : (
            <Button variant="text" onClick={() => setOnSendOTP(true)}>
              Send OTP
            </Button>
          )}
        </Box>

        {onSendOTP && (
          <>
            <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
              <TextField label="OTP" autoFocus fullWidth />
            </Box>

            <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
              <SecondaryButton sx={{ mr: 3 }} type="button" onClick={handleCancelForm}>
                <>{t`Cancel`}</>
              </SecondaryButton>

              <LoadingButton variant="contained">{t`Confirm`}</LoadingButton>
            </Box>
          </>
        )}
      </Card>
    );
  }

  if (formStatus === 'edit-orther') {
    return (
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" sx={{ alignItems: 'center' }}>
          <IconButton aria-label="Back" onClick={handleCancelForm}>
            <ArrowBackIosNewIcon />
          </IconButton>
          <Typography variant="h6">Fullname, nationality, country of residence</Typography>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <TextField label="Fullname" autoFocus fullWidth />
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <TextField label="Nationality" select fullWidth>
            {COUNTRIES.map((option, key) => (
              <MenuItem key={key} value={option.code}>
                <Box sx={{ width: 28, mr: 1 }} component="span">
                  <FlagIcon code={option.code} />
                </Box>
                {option.name}
              </MenuItem>
            ))}
          </TextField>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <TextField label="Country of residence" select fullWidth>
            {COUNTRIES.map((option, key) => (
              <MenuItem key={key} value={option.code}>
                <Box sx={{ width: 28, mr: 1 }} component="span">
                  <FlagIcon code={option.code} />
                </Box>
                {option.name}
              </MenuItem>
            ))}
          </TextField>
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
          <SecondaryButton sx={{ mr: 3 }} type="button" onClick={handleCancelForm}>
            <>{t`Cancel`}</>
          </SecondaryButton>

          <LoadingButton variant="contained">{t`Confirm`}</LoadingButton>
        </Box>
      </Card>
    );
  }

  return (
    <>
      <Card sx={{ p: 2, width: '100%' }}>
        <Box display="flex" justifyContent="space-between" sx={{ borderBottom: 1, borderColor: 'divider', pb: 1 }}>
          <Box sx={{ alignItems: 'center', mt: 2 }}>
            <Typography variant="h6">Email</Typography>
            <Typography sx={{ color: '#999999' }}>Hectagon@gmail.com</Typography>
          </Box>

          <Button sx={{ height: 'fit-content', mt: 1.5 }} variant="text" onClick={() => setFormStatus('edit-email')}>
            Edit
          </Button>
        </Box>

        <Box display="flex" justifyContent="space-between">
          <Box>
            <Box sx={{ alignItems: 'center', mt: 2 }}>
              <Typography variant="h6">Fullname</Typography>
              <Typography sx={{ color: '#999999' }}>John Smith</Typography>
            </Box>

            <Box sx={{ alignItems: 'center', mt: 2 }}>
              <Typography variant="h6">Nationality</Typography>
              <Typography sx={{ color: '#999999' }}>Vietnam</Typography>
            </Box>

            <Box sx={{ alignItems: 'center', mt: 2 }}>
              <Typography variant="h6">Country of residence</Typography>
              <Typography sx={{ color: '#999999' }}>Korea</Typography>
            </Box>
          </Box>

          <Button sx={{ height: 'fit-content', mt: 1.5 }} variant="text" onClick={() => setFormStatus('edit-orther')}>
            Edit
          </Button>
        </Box>
      </Card>
    </>
  );
};

export default InfoForm;
