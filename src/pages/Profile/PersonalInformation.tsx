import { useState } from 'react';
import { t } from '@lingui/macro';
import { useSearchParams } from 'react-router-dom';

import { Box, Typography } from '@mui/material';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabPanel from '@mui/lab/TabPanel';
import TabList from '@mui/lab/TabList';

import { ReactComponent as Info } from 'assets/icons/info.svg';
import { ReactComponent as Contact } from 'assets/icons/contact.svg';
import { ReactComponent as IdPaper } from 'assets/icons/id_paper.svg';
import avatar from 'assets/icons/avatar.png';

import InfoForm from './InfoForm';
import ContactForm from './ContactForm';
import IDForm from './IDForm';

const PersonalInformation = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const level = searchParams.get('level') || '1';

  const [tabInfo, setTabInfo] = useState(level);

  const handleChangeTabInfo = (event: React.SyntheticEvent, value: string) => {
    setTabInfo(value);
    setSearchParams({
      level: value,
    });
  };

  return (
    <>
      <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
        <Box component="img" src={avatar} />
      </Box>
      <Typography align="center">@riki</Typography>

      <Box sx={{ flexGrow: 1, display: 'flex', mt: 3 }}>
        <TabContext value={tabInfo}>
          <Box sx={{ borderRight: 1, borderColor: 'divider' }}>
            <TabList
              onChange={handleChangeTabInfo}
              orientation="vertical"
              variant="fullWidth"
              sx={{ width: 'max-content' }}
            >
              <Tab
                sx={{ justifyContent: 'left', minHeight: '30px' }}
                icon={<Info />}
                iconPosition="start"
                label={t`Who you are?`}
                value="1"
              />
              <Tab
                sx={{ justifyContent: 'left', minHeight: '30px' }}
                icon={<Contact />}
                iconPosition="start"
                label={t`How to contact?`}
                value="2"
              />
              <Tab
                sx={{ justifyContent: 'left', minHeight: '30px' }}
                icon={<IdPaper />}
                iconPosition="start"
                label={t`Your ID Paper`}
                value="3"
              />
            </TabList>
          </Box>

          <TabPanel value="1" sx={{ width: '100%' }}>
            <InfoForm />
          </TabPanel>
          <TabPanel value="2" sx={{ width: '100%' }}>
            <ContactForm />
          </TabPanel>
          <TabPanel value="3" sx={{ width: '100%' }}>
            <IDForm />
          </TabPanel>
        </TabContext>
      </Box>
    </>
  );
};

export default PersonalInformation;
