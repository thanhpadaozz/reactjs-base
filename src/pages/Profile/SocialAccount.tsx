import { t } from '@lingui/macro';

import { Box, Typography } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import AddCircleIcon from '@mui/icons-material/AddCircle';

import { ReactComponent as Twitter } from 'assets/icons/twitter.svg';
import avatar from 'assets/icons/avatar.png';

import useAppStore from 'stores/useAppStore';
import SecondaryButton from 'components/common/SecondaryButton';

const SocialAccount = () => {
  const { theme } = useAppStore();
  const SocialCard = () => {
    return (
      <Box display="flex" sx={{ justifyContent: 'space-between', alignItems: 'center', mt: 3 }}>
        <Box display="flex">
          <Box position="relative" sx={{ mr: 3 }}>
            <Box component="img" src={avatar} />
            <Box position="absolute" display="flex" sx={{ bottom: 0, right: 0 }}>
              <Twitter />
            </Box>
          </Box>

          <Box sx={{ alignItems: 'center', mt: 2 }}>
            <Typography variant="h6">Dau do</Typography>
            <Typography sx={{ color: '#999999' }}>12 Jan 2022</Typography>
          </Box>
        </Box>

        <SecondaryButton
          sx={{
            boxShadow: 'none',
            width: 150,
            height: 'fit-content',
            backgroundColor: (themeMui) =>
              theme === 'dark' ? `${themeMui.palette.grey[700]} !important` : `${themeMui.palette.grey[300]}`,
          }}
        >
          <>{t`Disconnect`}</>
        </SecondaryButton>
      </Box>
    );
  };
  return (
    <>
      <Box display="flex" sx={{ alignItems: 'center' }}>
        <Typography variant="h4">{t`4 Profile connected`}</Typography>
        <IconButton aria-label="Add">
          <AddCircleIcon />
        </IconButton>
      </Box>

      <Typography>{t`Connect your social networks and start with our campaign`}</Typography>

      <SocialCard />
      <SocialCard />
      <SocialCard />
    </>
  );
};

export default SocialAccount;
