/* eslint-disable @typescript-eslint/no-empty-function */
import create from 'zustand';
import { getItem, getToken, setItem } from 'helpers/storage';
import { setToken } from 'helpers/api';

interface AppContext {
  theme: string;
  setTheme: (theme?: string) => void;
  accessToken?: string;
  setAccessToken: (newToken: string) => void;
}

export interface IUseAppStore extends AppContext {
  setApp: (App: AppContext) => void;
}

const defaultTheme = getItem('theme');

const useAppStore = create<IUseAppStore>((set, get) => ({
  theme: defaultTheme,
  setTheme: (theme?: string) => {
    const newTheme = theme ? theme : get().theme == 'light' ? 'dark' : 'light';
    setItem('theme', newTheme);
    set({ theme: newTheme });
  },
  accessToken: getToken(),
  setAccessToken: (newToken: string) => {
    setToken(newToken);
    set({ accessToken: newToken });
  },
  setApp: (App) => set(() => App),
}));

export default useAppStore;
