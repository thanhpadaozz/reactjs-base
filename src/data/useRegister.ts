import { useMutation } from 'react-query';
import { api } from 'helpers/api';

interface IUseRegister {
  address: string;
  signature: string;
  username: string;
}
const useRegister = () =>
  useMutation(async ({ address, signature, username }: IUseRegister) => {
    const response = await api.post('/auth/register', {
      address,
      signature,
      username,
    });

    return response.data;
  });

export default useRegister;
