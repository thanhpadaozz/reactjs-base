import { useQuery } from 'react-query';

import useWeb3Store from 'stores/useWeb3Store';
import { api } from 'helpers/api';

const useGetNonce = () => {
  const { accountAddress } = useWeb3Store();
  return useQuery(['get nonce', accountAddress], async () => {
    if (!accountAddress) throw new Error('You are not connected!');

    const response = await api.get(`/auth/nonce/${accountAddress}`);

    return response.data;
  });
};

export default useGetNonce;
