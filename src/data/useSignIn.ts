import { useMutation } from 'react-query';
import { api } from 'helpers/api';

interface IUseSignIn {
  address: string;
  signature: string;
}
const useSignIn = () =>
  useMutation(async ({ address, signature }: IUseSignIn) => {
    const response = await api.post('/auth/login', {
      address,
      signature,
    });

    return response.data;
  });

export default useSignIn;
