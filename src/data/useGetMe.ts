import { useQuery } from 'react-query';
import { api } from 'helpers/api';
import useAppStore from 'stores/useAppStore';

const useGetProfile = () => {
  const { accessToken } = useAppStore();
  return useQuery(
    'get me',
    async () => {
      const response = await api.get(`/auth/me`);

      return response.data;
    },
    {
      staleTime: 3600,
      cacheTime: 3600,
      enabled: !!accessToken,
    },
  );
};

export default useGetProfile;
